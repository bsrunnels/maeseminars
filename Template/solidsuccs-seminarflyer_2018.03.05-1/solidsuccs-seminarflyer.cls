\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{solidsuccs-seminarflyer}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass{beamer}

\RequirePackage[orientation=portrait,size=a4,scale=2]{beamerposter}
\geometry{letterpaper,portrait}

\RequirePackage{ifthen}
\RequirePackage[absolute,overlay]{textpos}
\RequirePackage{roboto}
\RequirePackage[breakable,most]{tcolorbox}
\useinnertheme{default}
\RequirePackage{lipsum}
\RequirePackage{lmodern}
\RequirePackage{tcolorbox}
\RequirePackage{microtype}
\RequirePackage{setspace}
\definecolor{uccsgold}{HTML}{CFB87C}
\definecolor{uccsgray}{HTML}{ECECEC}


\beamertemplateballitem


\newcommand{\occasion}[2][]{\def\theshortoccasion{#1} \def\theoccasion{#2}}
\newcommand{\insertoccasion}{\ifdefined\theoccasion\theoccasion\fi}
\newcommand{\insertshortoccasion}{\ifdefined\theshortoccasion\theshortoccasion\fi}

\newcommand{\location}[2][]{\def\theshortlocation{#1} \def\thelocation{#2}}
\newcommand{\insertlocation}{\ifdefined\thelocation\thelocation\fi}
\newcommand{\insertshortlocation}{\ifdefined\theshortlocation\theshortlocation\fi}

\setlength{\TPHorizModule}{\paperwidth}
\setlength{\TPVertModule}{\paperheight}


\setbeamersize{text margin left=.45cm}
\setbeamertemplate{navigation symbols}{}


\setbeamertemplate{background}
{
    \includegraphics{solidsuccs-seminarflyer_images/background.pdf}
}


\define@key{Gin}{Trim} {\let\Gin@viewport@code\Gin@trim\expandafter\Gread@parse@vp#1 \\}

\newcommand\Title[1]{\def\@Title{#1}}
\newcommand\Speaker[1]{\def\@Speaker{#1}}
\newcommand\Institution[1]{\def\@Institution{#1}}
\newcommand\Abstract[1]{\def\@Abstract{#1}}
\newcommand\Biography[1]{\def\@Biography{#1}}
\newcommand\Date[1]{\def\@Date{#1}}
\newcommand\Time[1]{\def\@Time{#1}}
\newcommand\Location[1]{\def\@Location{#1}}
\newcommand\RoomNumber[1]{\def\@RoomNumber{#1}}
\newcommand\Photo[1]{\def\@Photo{#1}}

\newlength{\titleblock}
\newlength{\photoblock}
\setlength{\titleblock}{15.8cm}
\setlength{\photoblock}{4cm}

\renewcommand\maketitle{
\begin{textblock}{0.93}(0.03,0.15)
  \begin{minipage}{\linewidth}
    \begin{minipage}{\titleblock}
      \begin{tcolorbox}[enhanced jigsaw,arc=0pt,boxrule=0pt,borderline west={5pt}{0pt}{uccsgold},colback=uccsgray,height=5cm,width=\linewidth]
        \begin{minipage}[t][4.4cm]{\linewidth}
          \hyphenpenalty=10000
          \setstretch{1.75}
          \begin{flushleft}{ \bf \robotoslab \Large \@Title} \end{flushleft}
          \vfill
          \setstretch{1.3}
          {\bf \roboto \large \@Speaker}\\
          {\roboto \large \@Institution}
        \end{minipage}
      \end{tcolorbox}
    \end{minipage}
    \hfill
    \begin{minipage}{\photoblock}
      \includegraphics[Trim = \LeftBottomRightTopCrop, clip, width=\linewidth,height=5cm]{\@Photo}
    \end{minipage}
  \end{minipage}

  \begin{tcolorbox}[enhanced jigsaw,arc=0pt,boxrule=0pt,borderline west={5pt}{0pt}{uccsgold},colback=uccsgray,width=\linewidth]
    { \bf \robotoslab \large Abstract} 
    \vspace{.5cm}
    
    \roboto 
    \@Abstract

  \end{tcolorbox}

  \begin{tcolorbox}[enhanced jigsaw,arc=0pt,boxrule=0pt,borderline west={5pt}{0pt}{uccsgold},colback=uccsgray,width=\linewidth]
    { \bf \robotoslab \large Biography} 
    \vspace{.5cm}
    
    \roboto 
    \@Biography
  \end{tcolorbox}
\end{textblock}

\begin{textblock}{0.93}(0.03,0.93)
  \setstretch{1.5}
  {\color{white}\roboto \bf \Large \@Date  \hfill \@Location}

  {\color{white}\roboto  \Large \@Time \hfill \@RoomNumber}
\end{textblock}
}


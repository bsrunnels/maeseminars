-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: solidsuccs-seminarflyer
Binary: solidsuccs-seminarflyer
Architecture: any
Version: 2018.03.05-1
Maintainer: brunnels <brunnels@uccs.edu>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 3.9.6
Vcs-Git: git@github.com:bsrunnels/seminarflyer.git
Build-Depends: debhelper (>= 9)
Package-List:
 solidsuccs-seminarflyer deb base optional arch=any
Checksums-Sha1:
 23991c191884b64fb3fdfc01e223991c7ca525ed 25783 solidsuccs-seminarflyer_2018.03.05-1.tar.gz
Checksums-Sha256:
 808eb71712e31568881b51381c3ca3df3a42be742ab12f591ff96067c40b829e 25783 solidsuccs-seminarflyer_2018.03.05-1.tar.gz
Files:
 81f7633f2999a58da89460b3d303ef1e 25783 solidsuccs-seminarflyer_2018.03.05-1.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJanWu6AAoJEPVbuPxM3L0pBvEIAMvPXSrB2MB58pJ4mjUSB7YK
JWYiMl9Eyhjxhk9VX3taHIcvk64EOwJxOsRbIuKOqkYk4gDsrmh+knuxuEBMqHm8
yykG4Uh6BPE0DBItCrTGwemWilYGcRuDb02xNyHa9uFO2g7si+jerxBvToeY2zmr
DYcn3xLC0FIM941hEEt6KcsCMyh2Dm3HwKQxEbRtMJtK4KkURFfyPjzvU/uBQx92
rpafxZJYoU0WjvQbH4R0hk9ZpVzoB9og56+9MTxSKI1FN9FS5E4MK2ME1a+MF1Wo
83NNxji9nDZyEzlygCUIN8HBSVH2E9DCNVa85bXaq2NXNaefQSslHGOwW2eE9y0=
=KWey
-----END PGP SIGNATURE-----
